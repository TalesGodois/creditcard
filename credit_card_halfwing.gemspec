Gem::Specification.new do |s|
  s.name        = "hello_world"
  s.version     = "0.1.0"
  s.description = "A simple gem that says hello to the world!"
  s.summary     = "Say hello!"
  s.author      = "Tales Marinho"
  s.files       = Dir["{lib/**/*.rb,README.rdoc,test/**/*.rb,Rakefile,*.gemspec}"]
end